��          L      |       �      �   �   �      J  !   [     }  .  �     �  �   �     v  !   �     �                                         Really Simple CAPTCHA Really Simple CAPTCHA is a CAPTCHA module intended to be called from other plugins. It is originally created for my Contact Form 7 plugin. Takayuki Miyoshi https://contactform7.com/captcha/ https://ideasilo.wordpress.com/ PO-Revision-Date: 2019-07-04 17:34:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: ca
Project-Id-Version: Plugins - Really Simple CAPTCHA - Stable (latest release)
 Really Simple CAPTCHA Really Simple CAPTCHA és un mòdul CAPTCHA que és cridat a partir d'altres extensions. Originalment es va crear per a l'extensió Contact Form 7. Takayuki Miyoshi https://contactform7.com/captcha/ https://ideasilo.wordpress.com/ 