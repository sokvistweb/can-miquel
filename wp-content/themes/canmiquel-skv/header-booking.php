<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Entre l'Alt i el Baix Empordà, Cala Montgó és un clar exemple de l'autèntica Costa Brava, amb el Massís del Montgrí acabant abruptament en penya-segats emmarcant una platja de sorra fina." />
    <meta name="keywords" content="" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="canonical" href="http://www.canmiquel.com//">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">

    <?php wp_head(); ?>
    <!-- Modernizer Script for old Browsers -->		
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.js"></script>
    
    <script type="text/javascript" src="https://booking.canmiquel.com:4526/fwbookingresponsivestep/app/core/PSBookingEngine.1.3.js">
    </script>

</head>
<body <?php body_class(); ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
    
    
    <header>
		<div class="wrapper wrapper-header">
            
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/can-miquel_logo.svg" title="Hotel Can Miquel" alt="Hotel & Restaurant Can Miquel" width="115" height="75" class="logo"></a>
            
            <div class="hamburger hamburger--arrow-r">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            
			<nav id="nav_menu">
                <div class="language" id="dd">
                    <?php qtranxf_generateLanguageSelectCode('text') ?>
                </div>
				<?php html5blank_nav(); ?>
			</nav>
            
		</div>
	</header><!--  End Header  -->
    
