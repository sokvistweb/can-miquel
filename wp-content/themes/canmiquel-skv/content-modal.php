
<!-- Modal -->
<div id="modal" class="modal modal__bg" role="dialog" aria-hidden="true">
    <div class="modal__dialog">
        <div class="modal__content">
            <?php
            $id=37;
            $post = get_page($id);
            $content = apply_filters('the_content', $post->post_content);
            echo $content;
            ?>
            
            <!-- modal close button -->
            <a href="" class="modal__close cross-close">
                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a>
        </div>
    </div>
</div>

<div id="modal2" class="modal modal__bg" role="dialog" aria-hidden="true">
    <div class="modal__dialog">
        <div class="modal__content">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h2>Ofertes de feina</h2>
            <p>Envia'ns un e-mail a <a href="mailto:info@canmiquel.com">info@canmiquel.com</a></p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h2>Ofertas de trabajo</h2>
            <p>Mándanos un e-mail a <a href="mailto:info@canmiquel.com">info@canmiquel.com</a></p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h2>Job vacancies</h2>
            <p>Email us at <a href="mailto:info@canmiquel.com">info@canmiquel.com</a></p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='fr'): ?>
            <h2>Offre de travail</h2>
            <p>Envoyer un mail à <a href="mailto:info@canmiquel.com">info@canmiquel.com</a></p>
            <?php endif; ?>
            <?php } ?>

            <!-- modal close button -->
            <a href="" class="modal__close cross-close">
                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a>

        </div>
    </div>
</div>
<!-- /Modal -->