<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Entre l'Alt i el Baix Empordà, Cala Montgó és un clar exemple de l'autèntica Costa Brava, amb el Massís del Montgrí acabant abruptament en penya-segats emmarcant una platja de sorra fina." />
    <meta name="keywords" content="" />
    <meta name="author" content="Sokvist" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="canonical" href="https://www.canmiquel.com/">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    
    <!-- Open Graph -->
    <meta property="og:locale" content="ca_ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Hotel Can Miquel">
    <meta property="og:description" content="Entre l'Alt i el Baix Empordà, Cala Montgó és un clar exemple de l'autèntica Costa Brava, amb el Massís del Montgrí acabant abruptament en penya-segats emmarcant una platja de sorra fina.">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/og-image.jpg">
    <meta property="og:url" content="https://www.canmiquel.com/">
    <meta property="og:site_name" content="Hotel Can Miquel">

    <?php wp_head(); ?>
    <!-- Modernizer Script for old Browsers -->		
    <script async src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.js"></script>


</head>
<body <?php body_class(); ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
    
    <div class="eupopup eupopup-top"></div>
    
    <div class="eupopup-container eupopup-container-fixedtop"> 
        <div class="eupopup-markup">
            
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <div class="eupopup-head">Aquesta web fa servir cookies</div> 
            <div class="eupopup-body">Utilitzem cookies per assegurar-nos que us donem la millor experiència al nostre lloc web. Si continua navegant, considerem que accepta el seu ús. </div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Acceptar</a> 
                <a href="/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Més info</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <div class="eupopup-head">Este sitio web usa cookies</div> 
            <div class="eupopup-body">Usamos cookies para garantizar su mejor experiencia en nuestro sitio web. Si continúa navegando, consideramos que acepta su uso.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Aceptar</a> 
                <a href="/es/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Más info</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">>We use cookies to ensure that we give you the best experience on our website. By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="/en/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='fr'): ?>
            <div class="eupopup-head">This website is using cookies</div> 
            <div class="eupopup-body">>We use cookies to ensure that we give you the best experience on our website. By using this website, you agree to this.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                <a href="/fr/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
            </div> 
            <?php endif; ?>
            <?php } ?>
            
            <div class="clearfix"></div> 
            <a href="#" class="eupopup-closebutton">
                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a> 
        </div> 
    </div>
    
    <header>
		<div class="wrapper wrapper-header">
            
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/can-miquel_logo.svg" title="Hotel Can Miquel" alt="Hotel & Restaurant Can Miquel" width="115" height="75" class="logo"></a>
            
            <div class="hamburger hamburger--arrow-r">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            
			<nav id="nav_menu">
                <div class="language" id="dd">
                    <?php qtranxf_generateLanguageSelectCode('text') ?>
                </div>
				<?php html5blank_nav(); ?>
			</nav>
            
		</div>
	</header><!--  End Header  -->
    
