
<section class="booking">
    <div class="wrapper center">
        <div class="caption">

            <div class="flexbox-child flexbox-child-item-1">
                <div class="flexbox-container ">
                    <div class="flexbox-child-2 flexbox-child-2-item-1"><a class="webcam" href="webcam" title="Webcam a Montgó">Webcam</a></div>
                    <div class="flexbox-child-2 flexbox-child-2-item-2"><a class="googlemap" href="https://goo.gl/maps/zNjgib5DfKA2" title="Google Maps" target="_blank">
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        On som
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        Situación
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        Find us
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='fr'): ?>
                        Situation
                        <?php endif; ?>
                        <?php } ?>
                        </a></div>
                </div>
            </div>

            <div class="noflexbox">
                <div class="flexbox-child flexbox-child-item-2">
					<a href="#" class="getmore modal__trigger" data-modal="#modal">
                        <?php
                        $id=37;
                        $post = get_page($id);
                        $content = apply_filters('the_title', $post->post_title);
                        echo $content;
                        ?>
                    </a>
					
                    <div class="book">
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <p class="book-online"><strong>Reserva</strong></p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <p class="book-online"><strong>Reserva</strong></p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <p class="book-online"><strong>Booking</strong></p>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='fr'): ?>
                        <p class="book-online"><strong>Réservation</strong></p>
                        <?php endif; ?>
                        <?php } ?>
                        <ul class="book-contact">
                            <li><a class="phone" href="tel:0034972771452"><span>Telefon</span></a></li>
                            <li><a class="email" href="mailto:info@canmiquel.com"><span>info@canmiquel.com</span></a></li>
                        </ul>
                    </div>
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <form action="https://www.canmiquel.com/reserves" method="get">
                        <input name="channelkey" value="F670A1C3-E428-89A4-E040-D10AF416105C" type="hidden">
                        <input name="fullintegration" value="1" type="hidden">
                        <input name="vscroll" value="1" type="hidden">
                        <input name="idiomaid" value="ca" type="hidden">

                        Entrada <input name="desde" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="desde" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal1" class="calmini">

                        Sortida <input name="hasta" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="hasta" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal2" class="calmini">

                        <input name="boton" class="boto_reser" value="Reservar" type="submit">
                    </form>
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='es'): ?>
                    <form action="https://www.canmiquel.com/reserves" method="get">
                        <input name="channelkey" value="F670A1C3-E428-89A4-E040-D10AF416105C" type="hidden">
                        <input name="fullintegration" value="1" type="hidden">
                        <input name="vscroll" value="1" type="hidden">
                        <input name="idiomaid" value="es" type="hidden">

                        Entrada <input name="desde" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="desde" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal1" class="calmini">

                        Salida <input name="hasta" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="hasta" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal2" class="calmini">

                        <input name="boton" class="boto_reser" value="Reservar" type="submit">
                    </form>
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <form action="https://www.canmiquel.com/reserves" method="get">
                        <input name="channelkey" value="F670A1C3-E428-89A4-E040-D10AF416105C" type="hidden">
                        <input name="fullintegration" value="1" type="hidden">
                        <input name="vscroll" value="1" type="hidden">
                        <input name="idiomaid" value="en" type="hidden">

                        Check-in <input name="desde" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="desde" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal1" class="calmini">

                        Check-out <input name="hasta" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="hasta" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal2" class="calmini">

                        <input name="boton" class="boto_reser" value="Book" type="submit">
                    </form>
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='fr'): ?>
                    <form action="https://www.canmiquel.com/reserves" method="get">
                        <input name="channelkey" value="F670A1C3-E428-89A4-E040-D10AF416105C" type="hidden">
                        <input name="fullintegration" value="1" type="hidden">
                        <input name="vscroll" value="1" type="hidden">
                        <input name="idiomaid" value="fr" type="hidden">

                        Arrivée <input name="desde" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="desde" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal1" class="calmini">

                        Départ <input name="hasta" class="datepicker-here" data-language="es" data-position="right top" data-date-format="dd/mm/yy" id="hasta" value="" type="text">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/calendar-black.png" id="cal2" class="calmini">

                        <input name="boton" class="boto_reser" value="Réserver" type="submit">
                    </form>
                    <?php endif; ?>
                    <?php } ?>
                    
                </div>


                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                <a href="reserves/?channelkey=F670A1C3-E428-89A4-E040-D10AF416105C&idiomaid=ca" class="book-btn">
                Reserva Online
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                <a href="reserves/?channelkey=F670A1C3-E428-89A4-E040-D10AF416105C&idiomaid=es" class="book-btn">
                Reserva Online
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                <a href="reserves/?channelkey=F670A1C3-E428-89A4-E040-D10AF416105C&idiomaid=en" class="book-btn">
                Online Booking
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                <a href="reserves/?channelkey=F670A1C3-E428-89A4-E040-D10AF416105C&idiomaid=fr" class="book-btn">
                Réservation en ligne
                <?php endif; ?>
                <?php } ?>
                </a>

            </div>

            <div class="flexbox-child flexbox-child-item-3">
                <div class="flexbox-container ">
                    <div class="flexbox-child-2 flexbox-child-2-item-1">
                        <div class="cs-select cs-skin-circular">
                            <span class="follow cs-placeholder">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Segueix-nos
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Síguenos
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Follow us
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='fr'): ?>
                                Suivez-nous
                                <?php endif; ?>
                                <?php } ?>
                            </span>
                            <div class="cs-options">
                                <ul>
                                    <li><a class="facebook" href="<?php the_field('facebook', 'option'); ?>" target="_blank"></a></li>
                					<li><a class="instagram" href="<?php the_field('instagram', 'option'); ?>" target="_blank"></a></li>
									<li><a class="youtube" href="<?php the_field('youtube', 'option'); ?>" target="_blank"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="flexbox-child-2 flexbox-child-2-item-2"><a class="subscribe modal__trigger modal__trigger-small" data-modal="#modal2" href="#" title="Newsletter">
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        Subscriu-te
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        Suscríbete
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        Sign up
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='fr'): ?>
                        Signer
                        <?php endif; ?>
                        <?php } ?>
                        </a></div>
                </div>
            </div>

        </div><!-- /.caption -->
    </div>
</section>