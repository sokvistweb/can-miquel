<?php get_header(); ?>


    <section class="billboard halfheight height404">
        <div class="noslider">
            <div class="single-img bg-img-404"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin margin404">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <section class="intro wrapper">
            
            <h1><span><?php _e( 'Page not found', 'html5blank' ); ?></span></h1>
            
            <h2>
                <a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
            </h2>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
