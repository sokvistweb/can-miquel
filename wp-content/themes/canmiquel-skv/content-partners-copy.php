
<div class="partners">
    <ul class="logos">
        <li><a href="http://www.calamontgosup.com/" target="_blank" title="Cala Montgó SUP">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sup-cala-montgo-logo.png" alt="SUP Cala Montgó" width="364" height="220"></a>
            </a>
        </li>
        <li><a href="http://www.kayakingcostabrava.com/" target="_blank" title="Kayaking Costa Brava">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-KCB.png" alt="Kayaking Costa Brava" width="172" height="220"></a>
            </a>
        </li>
    </ul>
</div><!-- /.partners -->
