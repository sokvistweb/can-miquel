    
	<footer>
        <div class="wrapper wrapper-footer">
            <p class="contact-info"><span><?php the_field('address', 'option'); ?></span>
                <a class="googlemap-small" href="https://goo.gl/maps/zNjgib5DfKA2" title="Veure a Google maps" target="_blank">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="30px" height="30px" viewBox="156 -156 512 512" enable-background="new 156 -156 512 512" xml:space="preserve">
                    <g>
                        <path d="M412-124c-74,0-134.2,58.7-134.2,132.7c0,16.4,3.5,34.3,9.8,50.4h-0.1l0.6,1.2c0.5,1.1,1,2.2,1.5,3.3L412,324L533.8,64.9
                            l0.6-1.2c0.5-1.1,1.1-2.2,1.6-3.4l0.4-1.1c6.5-16.1,9.8-33.1,9.8-50.3C546.2-65.3,486-124,412-124z M412,50.9
                            c-25.9,0-46.9-21-46.9-46.9s21-46.9,46.9-46.9s46.9,21,46.9,46.9S437.9,50.9,412,50.9z"/>
                    </g>
                    </svg>
                    </a><br>
                Hotel <a href="tel:<?php the_field('tel_hotel_link', 'option'); ?>"><?php the_field('tel_hotel', 'option'); ?></a> · Restaurant <a href="tel:<?php the_field('tel_restaurant_link', 'option'); ?>"><?php the_field('tel_restaurant', 'option'); ?></a><br>
                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a> · <a href="http://www.canmiquel.com" title="La web de Can Miquel">www.canmiquel.com</a>
            </p>

            <ul class="social">
                <li><a class="facebook" href="https://www.facebook.com/canmiquelhotel/" target="_blank"></a></li>
                <li><a class="twitter" href="https://twitter.com/hotelcanmiquel" target="_blank"></a></li>
                <li><a class="pinterest" href="https://www.pinterest.com/canmiquelhotel/" target="_blank"></a></li>
                <li><a class="instagram" href="https://www.instagram.com/canmiquel/" target="_blank"></a></li>
            </ul>
        
            <div class="contact-form">
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <?php echo do_shortcode( '[contact-form-7 id="32" title="Contact form CAT"]' ); ?>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                    <?php echo do_shortcode( '[contact-form-7 id="33" title="Contact form ESP"]' ); ?>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                    <?php echo do_shortcode( '[contact-form-7 id="31" title="Contact form ENG"]' ); ?>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                    <?php echo do_shortcode( '[contact-form-7 id="34" title="Contact form FRA"]' ); ?>
                <?php endif; ?>
                <?php } ?>
            </div>
        </div>
        
        <div class="subfooter">
            <ul class="logos">
                <li><a href="https://www.costabravaverdhotels.com/" target="_blank" title="Hotels Costa Brava">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/costabravahotels_tallat.jpg" alt="Costa Brava Hotels" width="147" height="60"></a>
                    </a>
                </li>
                <li><a href="http://natura.costabrava.org/" target="_blank" title="Natura i Turisme Actiu">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/natura-i-turisme-actiu-logo.png" alt="Costa Brava Pirineu de Girona" width="220" height="60"></a>
                    </a>
                </li>
                <li>
                    <a href="http://www.viesverdes.cat/" target="_blank" title="Vies Verdes Girona">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/vies-verdes-girona.png" alt="Vies Verdes Girona" width="150" height="60">
                    </a>
                </li>
                <li>
                    <a href="http://www.viesverdes.cat/CA/1713/Serveis-turistics/Allotjaments/Hotel-Can-Miquel-html" target="_blank" title="Establiment de qualitat Bike & Bed">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bed-and-bike-logo.png" alt="Bike & Bed" width="78" height="60">
                    </a>
                </li>
            </ul>
            <p class="rights"><span>© <?php echo date('Y'); ?> Hotel Can Miquel</span> · <span>
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                    Núm. Registre Turístic HG-002087
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                    Nº. Registro Turístico HG-002087 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                    Tourist Registration no. HG-002087 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                    Nº d'enregistrement touristique HG-002087
                <?php endif; ?>
                <?php } ?>
                </span> · 
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <a href="avis-legal" title="Avís legal">Avís Legal</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                    <a href="avis-legal" title="Aviso legal">Aviso Legal</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                    <a href="avis-legal" title="Legal Notice">Legal Notice</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                    <a href="avis-legal" title="Mention Légale">Mention Légale</a> 
                <?php endif; ?>
                <?php } ?> ·
                
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <a href="politica-de-cookies" title="Política de cookies">Política de cookies</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                    <a href="politica-de-cookies" title="Política de cookies">Política de cookies</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                    <a href="politica-de-cookies" title="Cookies policy">Cookies policy</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                    <a href="politica-de-cookies" title="Politique de cookies">Politique de cookies</a> 
                <?php endif; ?>
                <?php } ?> ·
                
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <a href="politica-de-privacitat" title="Política de privacitat">Política de privacitat</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                    <a href="politica-de-privacitat" title="Política de privacidad">Política de privacidad</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                    <a href="politica-de-privacitat" title="Privacy policy">Privacy policy</a> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='fr'): ?>
                    <a href="politica-de-privacitat" title="Politique de confidentialité">Politique de confidentialité</a> 
                <?php endif; ?>
                <?php } ?>
                · <a href="http://www.sokvist.com/">Sokvist, Web & SEO</a>
            </p>
        </div>

        
        <a id="back-to-top" href="#"></a>
        
	</footer><!--  End Footer  -->



     <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/js/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/js/main.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-eu-cookie-law-popup.js"></script>

    <?php wp_footer(); ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-40218051-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>
</html>

