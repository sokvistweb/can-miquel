<?php /* Template Name: Page Empreses */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/empreses-01.jpg" alt="Empreses - Hotel Can Miquel" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'first-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/empreses-02.jpg" alt="Empreses - Hotel Can Miquel" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'second-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <p><?php echo get_post_meta($post->ID, 'content-bottom', true); ?></p>
            
            <span class="sep_line sep_bottom"></span>
        </section>
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
