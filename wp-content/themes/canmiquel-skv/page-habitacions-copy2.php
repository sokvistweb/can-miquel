<?php /* Template Name: Page Habitacions (old 2) */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

    <?php query_posts('post_type=page&p=5'); while (have_posts ()): the_post(); ?>
        <section class="intro wrapper">

            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>
            

            <span class="sep_line sep_bottom"></span>
            
        </section><!--  /intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-01b.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-02b.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-03b.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-04b.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-dobles-05.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-06b.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'first-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-01.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-dobles-01.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-dobles-02.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-dobles-03.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-dobles-04.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-06.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'second-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-07.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-08.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-09.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-10.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-11.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-12.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'third-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-13.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-14.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-15.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-16.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-17.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rooms/habitacions-18.jpg" alt="Les habitacions - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'fourth-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->

        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <p><?php echo get_post_meta($post->ID, 'content-bottom', true); ?></p>
            
            <span class="sep_line sep_bottom"></span>
        </section>
        
    <?php endwhile; ?>    
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
