<?php get_header(); ?>


	<section class="billboard fullheight">
        <div class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                </ul>
            </div> <!-- /.flexslider -->
        </div> <!-- /.slider -->
    </section><!-- /.billboard  -->
    
    <main class="fullmargin">
        
        
        <?php get_template_part( 'content', 'booking' ); ?>
        
        
        <section class="intro wrapper">
            
            <?php query_posts('post_type=page&p=2'); while (have_posts ()): the_post(); ?>

                    <?php the_content(); ?>
            
            <?php endwhile; ?>
            
            <div id="video-play">
				<a class="video" href="https://www.youtube.com/watch?v=4KDrpQQNr30" data-poptrox="youtube,800x480"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-video-image.jpg" alt="" title="Video - Hotel Can Miquel - Cala Montgó, L'Escala" /></a>
            </div>
            
            <?php get_template_part( 'content', 'totem' ); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Features  -->


        <section class="info">
            
            <figure class="square-box">
                <div class="square-content">
                    <div class="square-table">
                        <div class="slider">
                            <div class="flexslider-square">
                                <ul class="slides">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/habitacions-home-1.jpg"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/habitacions-home-2.jpg"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/habitacions-home-3.jpg"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/habitacions-home-4.jpg"></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/habitacions-home-5.jpg"></li>
                                </ul>
                            </div> <!-- /.flexslider-square -->
                        </div> <!-- /.slider -->
                    </div>
                </div>
            </figure>
            
            <div class="square-box">
                <div class="square-content with-border">
                    <div class="square-table">
                        <div class="info_details">
                            <?php query_posts('post_type=page&p=5'); while (have_posts ()): the_post(); ?>
                            <h3><?php the_title(); ?></h3>
                            <?php echo get_post_meta($post->ID, 'front-page-content', true); ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo get_post_meta($post->ID, 'page-link', true); ?></a>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <figure class="square-box effect-lily">
                <div class="square-content">
                    <img class="slide-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/restaurant.jpg">
                    <figcaption>
                        <?php query_posts('post_type=page&p=7'); while (have_posts ()): the_post(); ?>
                        <div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View more</a>
                        <?php endwhile; ?>
                    </figcaption>
                </div>
            </figure>
        
            <figure class="square-box effect-bubba">
                <div class="square-content">
                    <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/la-bassa.jpg">
                    <figcaption>
                        <?php query_posts('post_type=page&p=9'); while (have_posts ()): the_post(); ?>
                        <div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View more</a>
                        <?php endwhile; ?>
                    </figcaption>
                </div>
            </figure>
            
            <div class="square-box">
                <div class="square-content">
                    <div class="square-table">
                        <div class="info_details">
                            
                            <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-front-page')) ?>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <figure class="square-box effect-lily">
                <div class="square-content">
                    <img class="slide-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/serveis.jpg">
                    <figcaption>
                        <?php query_posts('post_type=page&p=11'); while (have_posts ()): the_post(); ?>
                        <div>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View more</a>
                        <?php endwhile; ?>
                    </figcaption>
                </div>
            </figure>
            
            <figure class="square-box effect-lily">
                <div class="square-content">
                    <img class="slide-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/galeria.jpg">
                    <figcaption>
                        <?php query_posts('post_type=page&p=15'); while (have_posts ()): the_post(); ?>
                        <div>
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View more</a>
                        <?php endwhile; ?>
                    </figcaption>
                </div>
            </figure>

            <figure class="square-box">
                <div class="square-content">
                    <div class="square-table">
                        <section class="video-bg">
                            <?php query_posts('post_type=page&p=13'); while (have_posts ()): the_post(); ?>
                            <div class="inner">
                                <div>
                                    <h3><?php the_title(); ?></h3>
                                </div>
                            </div>
                            <div id="video-viewport">
                                <video muted loop>
                                    <source src="<?php echo get_template_directory_uri(); ?>/assets/images/turisme-actiu-6.mp4" type="video/mp4" />
                                </video>
                            </div>
                            <div id="img-viewport">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg_video.jpg">
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">View more</a>
                            <?php endwhile; ?>
                        </section>
                    </div>
                </div>
            </figure>

        </section><!--  End Info  -->


        <section class="testimonials wrapper">

            <span class="sep_line sep_top">
            </span>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/tripadvisor-owl.png" alt="">
            <h2>Tripadvisor</h2>
            
            <div class="tslider">
                <ul>
                    <?php if (have_posts()) : ?>
                    <?php query_posts(array( 'post_type' => 'testimonials', 'posts_per_page' => 6 )); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <li>
                        <div class="slide-text">
                            <p class="author"><?php the_title(); ?></p>
                            <?php the_content(); ?>
                        </div>
                    </li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div> <!-- /.tslider -->
            
            <span class="sep_line sep_bottom"></span>

        </section><!--  End Testimonials  -->
        
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
