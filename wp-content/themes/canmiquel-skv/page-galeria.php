<?php /* Template Name: Page Galeria */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
