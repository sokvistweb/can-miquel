<?php /* Template Name: Page Turisme Actiu (old) */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight only-img">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-01.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-02.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-03.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-04.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-05.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/turisme-actiu-06.jpg" alt="Turisme actiu - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>

                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'first-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <?php get_template_part( 'content', 'partners' ); ?>
            
            <span class="sep_line sep_bottom"></span>
        </section><!--  End After  -->
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
