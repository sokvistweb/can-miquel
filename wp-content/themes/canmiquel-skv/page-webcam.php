<?php /* Template Name: Page Webcam */ get_header(); ?>



    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>


        <section class="intro wrapper">

            <h1>Webcam</h1>
            <div class="webcam-wrapper">
                <!--<img id="webcam-img" src="http://5.1.38.49:5550/GetImage.cgi?CH=1">-->
				<img id="webcam-img" src="https://raclus.net/webcams/canmiquel/1">
            </div>
            

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Features  -->
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
