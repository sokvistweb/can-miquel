<?php /* Template Name: Page Habitacions (old) */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

    <?php query_posts('post_type=page&p=5'); while (have_posts ()): the_post(); ?>
        <section class="intro wrapper">

            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>
            

            <span class="sep_line sep_bottom"></span>
            
        </section><!--  /intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/habitacions-01.jpg" alt="Les habitacions" />
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/habitacions-02.jpg" alt="Les habitacions" />
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/habitacions-03.jpg" alt="Les habitacions" />
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/habitacions-04.jpg" alt="Les habitacions" />
                                </li>
                                <li>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/habitacions-05.jpg" alt="Les habitacions" />
                                </li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </div>
            </div><!-- /.spotlight -->

        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <p><?php echo get_post_meta($post->ID, 'content-bottom', true); ?></p>
            
            <span class="sep_line sep_bottom"></span>
        </section>
        
    <?php endwhile; ?>    
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
