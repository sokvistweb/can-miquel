<?php /* Template Name: Page La Bassa (old) */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-02.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-03.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-04.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-05.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-07.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/gastronomia-la-brasa-06.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'first-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-07.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-08.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-09.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-10.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-11.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/la-bassa-13.jpg" alt="La Bassa - Hotel Can Miquel" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php echo get_post_meta($post->ID, 'second-slider-text', true); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <?php get_template_part( 'content', 'totem' ); ?>
            
            <span class="sep_line sep_bottom"></span>
        </section><!--  End After  -->
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
