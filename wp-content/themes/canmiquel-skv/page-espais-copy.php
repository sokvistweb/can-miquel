<?php /* Template Name: Page Espais (old) */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
        <section class="page-wrapper">
            <div class="spotlight only-img">
                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/espais-01.jpg" alt="Les habitacions" />
                </div>

                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/espais-02.jpg" alt="Les habitacions" />
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight only-img">
                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/espais-03.jpg" alt="Les habitacions" />
                </div>
                
                <div class="image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pages/espais-04.jpg" alt="Les habitacions" />
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <?php get_template_part( 'content', 'totem' ); ?>
            
            <span class="sep_line sep_bottom"></span>
        </section><!--  End After  -->
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
