
<aside class="col-aside">
    
    <div class="widget widget-subscribe">
        <?php if(function_exists('qtranxf_getLanguage')) { ?>
        <?php if (qtranxf_getLanguage()=='ca'): ?>
        <h3 class="widget-title">Com tenir una pàgina d’inici súper eficaç</h3>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='es'): ?>
        <h3 class="widget-title">Cómo tener una página de inicio súper eficaz</h3>
        <?php endif; ?>
        <?php if (qtranxf_getLanguage()=='en'): ?>
        <h3 class="widget-title">How to have a super efficient homepage</h3>
        <?php endif; ?>
        <?php } ?>
        <!-- Mailchimp subscribe form -->
        <div id="mc_embed_signup" class="subscribe-widgetbar">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <p>Subscriu-te i descarrega’t aquí la guia</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <p>Suscríbete y descargate aquí la guía</p>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <p>Sign up and download the guide here</p>
            <?php endif; ?>
            <?php } ?>
            <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=f29df77dd0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">

                <div class="mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                </div>
                <div class="mc-field-group">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='es'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                <div class="clear">
                    <?php if(function_exists('qtranxf_getLanguage')) { ?>
                    <?php if (qtranxf_getLanguage()=='ca'): ?>
                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='es'): ?>
                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php if (qtranxf_getLanguage()=='en'): ?>
                    <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                    <?php endif; ?>
                    <?php } ?>
                </div>
                </div>
            </form>
        </div>
        <!-- /Mailchimp subscribe form -->
    </div>
    
    
    <div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-blog')) ?>
	</div>
    
    
</aside>


