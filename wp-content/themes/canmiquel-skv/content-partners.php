
<div class="partners">
    <ul class="logos">
		<?php if( have_rows('logos', 'option') ): ?>
		<?php while( have_rows('logos', 'option') ): the_row(); ?>
        <li>
			<?php
			$image = get_sub_field('logo');
			$url = get_sub_field('link');
			if( $image ):

            // Image variables.
            $title = $image['title'];
            $alt = $image['alt'];

            // Thumbnail size attributes.
            $size = 'thumbnail';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
        	?>

            <a href="<?php echo esc_url($url); ?>" title="<?php echo esc_attr($title); ?>" target="_blank">
                <img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />
            </a>
			<?php endif; ?>
        </li>
		<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
    </ul>
</div><!-- /.partners -->
