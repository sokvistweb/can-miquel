<?php /* Template Name: Page Gastronomia */ get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="single-img bg-img-1"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <?php get_template_part( 'content', 'booking' ); ?>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <section class="intro wrapper">
            
            <h1><?php the_title(); ?></h1>
            
            <?php the_content(); ?>

            <span class="sep_line sep_bottom"></span>
        </section><!--  End Intro  -->
        
		
        <section class="page-wrapper">
            <?php if( have_rows('section') ): ?>
			<?php while( have_rows('section') ): the_row(); ?>
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                            <?php if( have_rows('images') ): ?>
                            <?php while( have_rows('images') ): the_row(); ?>
                                <li>
                                    <?php $img = get_sub_field('image'); ?>
                                    <?php if( $img ): ?>
                                    <img src="<?php echo $img['sizes']['medium_large']; ?>" width="<?php echo $img['sizes']['medium_large-width']; ?>" height="<?php echo $img['sizes']['medium_large-height']; ?>" alt="<?php echo $img['alt']; ?>" />
                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <?php the_sub_field('content'); ?>
                    </div>
                </div>
            </div><!-- /.spotlight -->
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
        </section>
		
        
        <section class="after wrapper">
            <span class="sep_line sep_top"></span>

            <?php if ( 'yes' == get_field( 'show_logos' ) ): ?>
				<?php get_template_part( 'content', 'partners' ); ?>
			<?php else: ?>
				<?php get_template_part( 'content', 'totem' ); ?>
			<?php endif; ?>
            
            <span class="sep_line sep_bottom"></span>
        </section><!--  End After  -->
        
        <?php endwhile; ?>
        <?php endif; ?>
        
    </main>


    <?php get_template_part( 'content', 'modal' ); ?>


<?php get_footer(); ?>
